package uk.co.inzent.signup.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class SignupForm extends ActionBarActivity {

    private List<baseValidator> controls;

    public SignupForm() {
     this.controls = new ArrayList<baseValidator>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_form);
        // set up form validation objects
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.nameBox),
                        "name",
                        "Name is invalid"
                )
        );
        controls.add(
                new emailValidator(
                        (EditText)findViewById(R.id.emailBox),
                        "email",
                        "E. mail is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.landLineBox),
                        "landline",
                        "Land line number is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.mobileBox),
                        "mobile",
                        "Mobile number is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.address1Box),
                        "address1",
                        "First line of address is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.address2Box),
                        "address2",
                        "Second line of address is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.townBox),
                        "town",
                        "Town is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText)findViewById(R.id.countyBox),
                        "county",
                        "County is invalid"
                )
        );
        controls.add(
                new baseValidator(
                        (EditText) findViewById(R.id.postcodeBox),
                        "postcode",
                        "Postcode is invalid"
                )
        );
    }

    public void clearBtnClick (View v) {
        for (baseValidator control : this.controls) {
            control.clear();
        }
    }

    public void signupBtnClick (View v) throws IOException {
        // We can either waste memory building the data list during validation
        // only to throw it away again when data is invalid
        // OR waste time parsing the input controls twice - once to validate, once to post
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        boolean dataIsGood = true; // Innocent until proved guilty
        for (baseValidator control : this.controls) {
            nameValuePairs.add(control.httpData());
            if (control.isInvalid()) {
                dataIsGood = false;
            }
        }
        if (dataIsGood) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.edge-effect.co.uk/test.php");
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            }
            catch (UnsupportedEncodingException e) {
                // This "should never" happen. And I'm not worrying about it for now
            }
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);
            Toast.makeText(
                    getApplicationContext(),
                    responseBody,
                    Toast.LENGTH_LONG
            ).show();



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.signup_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
