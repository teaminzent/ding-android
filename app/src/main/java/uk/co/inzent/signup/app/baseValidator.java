package uk.co.inzent.signup.app;

import android.widget.EditText;

import org.apache.http.message.BasicNameValuePair;

public class baseValidator {

    protected EditText control;
    protected String httpName;
    protected CharSequence errorMessage;

    public baseValidator(EditText control, String httpName, CharSequence errorMessage) {
        this.control = control;
        this.httpName = httpName;
        this.errorMessage = errorMessage;
    }

    public void clear() {
        this.control.setText("");
    }

    public BasicNameValuePair httpData () {
        return new BasicNameValuePair(this.httpName, this.control.getText().toString());
    }

    // Override this to implement real validators
    public boolean validate(CharSequence target) {
        // the base validator just passes everything
        return true;
    }

    public boolean isInvalid () {
        if (this.validate(this.control.getText())) {
            this.control.setError(null);
            return false;
        }
        else {
            this.control.setError(this.errorMessage);
            return true;
        }
    }
}
