package uk.co.inzent.signup.app;

import android.text.TextUtils;
import android.widget.EditText;

public class emailValidator extends baseValidator {

    public emailValidator(EditText control, String httpName, CharSequence errorMessage) {
        super(control, httpName, errorMessage);
    }

    @Override
    public boolean validate(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
